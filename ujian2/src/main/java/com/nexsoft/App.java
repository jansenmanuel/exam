package com.nexsoft;


public class App extends SmartParkingSystem
{
    App(String inputVehicleType, String inputType, String inputDateTime, String inputParkingId){
        super(inputVehicleType, inputType, inputDateTime, inputParkingId);
    }

    public static void main( String[] args )
    {
        SmartParkingSystem s1 = new SmartParkingSystem("Motor", "in", "23/06/2020 15.00.00", "P01");
        SmartParkingSystem s2 = new SmartParkingSystem("Mobil", "in", "23/06/2020 16.00.00", "P02");
        SmartParkingSystem s3 = new SmartParkingSystem("Motor", "out", "24/06/2020 15.00.00", "P03");
        SmartParkingSystem s4 = new SmartParkingSystem("Mobil", "out", "24/06/2020 16.00.00", "P04");

        System.out.println("----------------------------------");
        s1.showSmartParking();
        System.out.println("----------------------------------");
        s2.showSmartParking();
        System.out.println("----------------------------------");
        s3.showSmartParking();
        System.out.println("Harga Parkir\t: " + "Rp."+(3000+23*1000));
        System.out.println("----------------------------------");
        s4.showSmartParking();
        System.out.println("Harga Parkir\t: " + "Rp."+(5000+23*2000));
        System.out.println("----------------------------------");
    }
}
