package com.nexsoft;

public class SmartParkingSystem extends Vehicle{
    SmartParkingSystem(String inputVehicleType, String inputType, String inputDateTime, String inputParkingId){
        super.setVehicleType(inputVehicleType);
        super.setType(inputType);
        super.setDateTime(inputDateTime);
        super.setParkingId(inputParkingId);
    }

    public void showSmartParking(){
        System.out.println("Tipe kendaraan\t: " + super.getVehicleType());
        System.out.println("IN / OUT\t\t: " + super.getType());
        System.out.println("Date and Time\t: " + super.getDateTime());
        System.out.println("Parking ID\t\t: " + super.getParkingId());
    }
}
