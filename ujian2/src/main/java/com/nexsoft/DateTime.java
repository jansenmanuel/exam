package com.nexsoft;

import java.time.LocalDateTime;

public class DateTime extends Parking{
    String showDateTime;

    public void setDateTime(String inputDateTime){
        this.showDateTime = inputDateTime;
    }

    public String getDateTime(){
        return this.showDateTime;
    }
}